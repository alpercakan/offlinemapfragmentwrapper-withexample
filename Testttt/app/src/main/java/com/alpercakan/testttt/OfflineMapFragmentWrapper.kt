package com.alpercakan.testttt

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Tile
import com.google.android.gms.maps.model.TileOverlayOptions
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.ByteBuffer


/**
 * Tile sizes
 */
private val STANDART_TILE_WIDTH = 256 // px
private val STANDART_TILE_HEIGHT = 256 // px
private val RETINA_TILE_WIDTH = 512 // px
private val RETINA_TILE_HEIGHT = 512 // px

private val DEFAULT_ZOOM_LEVEL_FOR_REGIONAL_CACHES = 17

enum class TileSource { GOOGLE_MAPS, OPEN_STREET_MAP }

/**
 * A wrapper class for MapFragment from Maps SDK.
 *
 * Provides tile caching capabilities with optional tile sources, such as OSM or Google Maps API.
 *
 * @param mapFragment Map fragment object to be wrapped. Provide null if the wrapper will be used
 * for tasks like pre-caching or cache cleaning; tasks which do not require an MapFragment object.
 * @param cacheReader A function which will return the cached tile as ByteArray, on success; and
 * will return null on failure.
 * @param cacheWriter A function which will save the cached tile which is provided as ByteArray. The
 * function must return true on success, false on failure.
 * @param tileFetcher A function which can retrieve binary data as ByteArray from the given URL.
 * Since MapFragment already retrieves tiles in a new thread, tileFetcher function must be
 * blocking (synchronized).
 */
class OfflineMapFragmentWrapper (val mapFragment: MapFragment?,
                                 val cacheWriter : (cacheName : String, data : ByteArray) -> Boolean,
                                 val cacheReader : (cacheName : String) -> ByteArray?,
                                 val tileFetcher : (url : String) -> ByteArray?,
                                 var useRetinaTiles : Boolean = true,
                                 val tileSource : TileSource = TileSource.GOOGLE_MAPS) {
    var map : GoogleMap? = null
        private set

    /**
     * Creates and properly wraps the map.
     * @param onMapReadyCallback Function to be called when the map is ready to be used.
     */
    fun createMap(onMapReadyCallback : (() -> Unit)? = null) {
        if (mapFragment == null) {
            throw IllegalStateException()
        }

        mapFragment.getMapAsync {
            map = it

            // Since we will be providing rendered tiles, map itself should not render anything.
            it.mapType = GoogleMap.MAP_TYPE_NONE

            it.addTileOverlay(TileOverlayOptions().tileProvider {
                x: Int, y: Int, zoom: Int ->
                if (useRetinaTiles) getRetinaTile(x, y, zoom) else getTile(x, y, zoom) })

            if (onMapReadyCallback != null)
                onMapReadyCallback()
        }
    }

    /**
     * Throws an appropriate exception if given tile parameters are illegal.
     * @param x Tile columnn
     * @param y Tile row
     * @param zoom Zoom level
     */
    private fun throwIfTileParamsIllegal(x: Int, y: Int, zoom: Int) {
        // Max zoom level depends on position. Although not a tight one, 29 is a safe bound.
        if (zoom !in 0..29)
            throw IllegalArgumentException("Zoom level exceeds bounds.")

        if (x !in 0 until (1L shl zoom /* 2^zoom */))
            throw IllegalArgumentException("Invalid column number")

        if (y !in 0 until (1L shl zoom /* 2^zoom */))
            throw IllegalArgumentException("Invalid row number")
    }

    /**
     * Creates a name for the tile.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return Tile name
     */
    private fun createTileName(x: Int, y: Int, zoom: Int) : String {
        throwIfTileParamsIllegal(x, y, zoom)

        return "OfflineMapFragmentWrapper-tilecache-$x-$y-$zoom"
    }

    /**
     * Fetches a tile from the source.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return Data on success, null on failure
     */
    private fun fetchTile(x: Int, y: Int, zoom: Int) = tileFetcher(when (tileSource) {
        TileSource.GOOGLE_MAPS -> "http://mt3.google.com/vt/lyrs=m&x=$x&y=$y&z=$zoom"
        TileSource.OPEN_STREET_MAP -> "http://a.tile.openstreetmap.org/$zoom/$x/$y.png"
    })

    /**
     * Caches a tile.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return True on success, false on failure
     */
    private fun cacheTile(x: Int, y: Int, zoom: Int) : Boolean {
        throwIfTileParamsIllegal(x, y, zoom)

        val data = fetchTile(x, y, zoom)

        if (data == null) {
            return false
        }

        return cacheWriter(createTileName(x, y, zoom), data)
    }

    /**
     * Caches all tiles for the given rectangular region.
     *
     * Since tileFetcher is a blocking network function, this function should never be called on
     * the main thread.
     *
     * @param bounds Geographical bounds of the region
     * @param minZoomLevel Minimum zoom level
     * @param maxZoomLevel Maximum zoom levels
     */
    fun cacheRegion(bounds : LatLngBounds, minZoomLevel : Int, maxZoomLevel : Int) : Boolean {
        for (zoomLevel in minZoomLevel..maxZoomLevel) {
            if (!cacheRegion(bounds, zoomLevel)) {
                return false
            }
        }

        return true
    }

    /**
     * Caches all tiles of the given zoom level for the given rectangular region.
     *
     * Since tileFetcher is a blocking network function, this function should never be called on
     * the main thread.
     *
     * @param bounds Geographical bounds of the region
     * @param zoomLevel Zoom level for caching
     */
    fun cacheRegion(bounds : LatLngBounds, zoomLevel : Int = DEFAULT_ZOOM_LEVEL_FOR_REGIONAL_CACHES)
            : Boolean {

        val southwestTileParam = getTileNumber(bounds.southwest.latitude,
                bounds.southwest.longitude,
                zoomLevel)
        val northeastTileParam = getTileNumber(bounds.northeast.latitude,
                bounds.northeast.longitude,
                zoomLevel)

        for (x in southwestTileParam.first..northeastTileParam.first) {
            for (y in northeastTileParam.second..southwestTileParam.second) {
                if (!cacheTile(x, y, zoomLevel)) {
                    return false
                }
            }
        }

        return true
    }

    /**
     * Retrieves a tile from cache.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return Null if cached tile could not be retrieved (probably cache does not exist for that
     * file); tile itself otherwise.
     */
    private fun getCachedTile(x: Int, y: Int, zoom: Int) : Tile? {
        throwIfTileParamsIllegal(x, y, zoom)

        var data = cacheReader(createTileName(x, y, zoom))

        if (data == null) {
            return null
        } else {
            return Tile(STANDART_TILE_WIDTH, STANDART_TILE_HEIGHT, data)
        }
    }

    /**
     * Obtains a standard quality tile.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return Null if the tile can not be provided; tile itself otherwise.
     */
    private fun getTile(x: Int, y: Int, zoom: Int) : Tile? {
        throwIfTileParamsIllegal(x, y, zoom)

        var tile = getCachedTile(x, y, zoom)

        if (tile == null) {
            cacheTile(x, y, zoom)
            tile = getCachedTile(x, y, zoom)
        }

        return tile
    }

    /**
     * Obtains a high quality tile.
     * @param x Column number of the tile. Must be in the range [0, 2^zoom - 1]
     * @param y Row number of the file. Must be in the range [0, 2^zoom - 1]
     * @param zoom Zoom level. Must be greater than 0. Maximum value depends on position.
     * @return Null if the tile can not be provided; tile itself otherwise.
     */
    private fun getRetinaTile(x: Int, y: Int, zoom: Int) : Tile? {
        val tile1 = getTile(2*x + 1, 2*y, zoom + 1)
        val tile2 = getTile(2*x, 2*y, zoom + 1)
        val tile3 = getTile(2*x, 2*y + 1, zoom + 1)
        val tile4 = getTile(2*x + 1, 2*y + 1, zoom + 1)

        if (tile1 == null || tile2 == null || tile3 == null || tile4 == null) {
            return null
        }

        var retinaTileImage = mergeTileImages(tile1.data, tile2.data, tile3.data, tile4.data)

        if (retinaTileImage == null) {
            return null
        }

        return Tile(RETINA_TILE_WIDTH, RETINA_TILE_HEIGHT, retinaTileImage)
    }

    /**
     * Merges given tile images into one image.
     *
     * ---------
     * | 2 | 1 |
     * |-------|
     * | 3 | 4 |
     * ---------
     */
    private fun mergeTileImages(tile1 : ByteArray, tile2 : ByteArray,
                                tile3 : ByteArray, tile4 : ByteArray) : ByteArray? {
        val bitmap1 = BitmapFactory.decodeByteArray(tile1, 0, tile1.size)
        val bitmap2 = BitmapFactory.decodeByteArray(tile2, 0, tile2.size)
        val bitmap3 = BitmapFactory.decodeByteArray(tile3, 0, tile3.size)
        val bitmap4 = BitmapFactory.decodeByteArray(tile4, 0, tile4.size)
        val retinaBitmap = Bitmap.createBitmap(RETINA_TILE_WIDTH, RETINA_TILE_HEIGHT, bitmap1.config)

        if (bitmap1 == null || bitmap2 == null || bitmap3 == null ||
            bitmap4 == null || retinaBitmap == null) {
            return null
        }

        val canvas = Canvas(retinaBitmap)

        canvas.drawBitmap(bitmap1, null,
                          Rect(STANDART_TILE_WIDTH, 0, STANDART_TILE_WIDTH*2, STANDART_TILE_HEIGHT),
                          null)

        canvas.drawBitmap(bitmap2, null,
                          Rect(0, 0, STANDART_TILE_WIDTH, STANDART_TILE_HEIGHT),
                          null)
        canvas.drawBitmap(bitmap3, null,
                          Rect(0, STANDART_TILE_HEIGHT, STANDART_TILE_WIDTH, STANDART_TILE_HEIGHT*2),
                          null)
        canvas.drawBitmap(bitmap4, null,
                          Rect(STANDART_TILE_WIDTH, STANDART_TILE_HEIGHT, STANDART_TILE_WIDTH*2, STANDART_TILE_HEIGHT*2),
                          null)

        val byteStream = ByteArrayOutputStream()

        retinaBitmap.compress(Bitmap.CompressFormat.PNG, 100 /* ignored for PNG */, byteStream)

        return byteStream.toByteArray()
    }

    /**
     * Calculates row and column of the tile which bounds the given point on the given zoom level.
     * @return (x, y)
     */
    private fun getTileNumber(lat: Double, lon: Double, zoom: Int) : Pair<Int, Int> {
        var x = Math.floor((lon + 180) / 360 * (1 shl zoom)).toInt()
        var y = Math.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1
                / Math.cos(Math.toRadians(lat))) / Math.PI) / 2
                * (1 shl zoom)).toInt()
        if (x < 0)
            x = 0
        if (x >= 1 shl zoom)
            x = (1 shl zoom) - 1
        if (y < 0)
            y = 0
        if (y >= 1 shl zoom)
            y = (1 shl zoom) - 1

        return Pair(x, y)
    }
}