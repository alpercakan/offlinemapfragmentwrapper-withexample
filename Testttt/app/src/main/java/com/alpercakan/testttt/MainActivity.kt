package com.alpercakan.testttt

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment

import java.io.FileInputStream
import android.os.AsyncTask
import android.view.View
import android.widget.CheckBox
import java.io.File


class MainActivity : AppCompatActivity() {

    lateinit var mapFragment: MapFragment
    lateinit var map : GoogleMap
    lateinit var textView : TextView
    lateinit var retinaCheckBox : CheckBox
    var cacheByteCount : Long = 0

    fun updateShownCacheSize(v : View) {
        if (v !== findViewById(R.id.updateCacheSizeButton)) {
            return
        }

        textView.setText("$cacheByteCount bytes = ${cacheByteCount / 1024.0} KB")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textView = findViewById (R.id.cacheTextView) as TextView

        mapFragment = fragmentManager
                .findFragmentById(R.id.mapfrag) as MapFragment

        val wrapper = OfflineMapFragmentWrapper(mapFragment,
                this::cacheWriter ,
                this::cacheReader,
                this::getFileFromURL, false, TileSource.GOOGLE_MAPS)


        wrapper.createMap ()

        retinaCheckBox = findViewById(R.id.retinaCheckBox) as CheckBox

        retinaCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
            wrapper.useRetinaTiles = isChecked
            wrapper.createMap()
        }
    }

    fun cacheWriter(cacheName : String,  data : ByteArray) : Boolean
    {
        var output = openFileOutput("$cacheName.png", Context.MODE_PRIVATE)

        output.write(data)
        output.close()

        cacheByteCount += data.size

        return  true
    }


    fun cacheReader(cacheName: String) : ByteArray?
    {
        var input : FileInputStream? = null
        var data : ByteArray?

        try {
            input = openFileInput("$cacheName.png")

            data = org.apache.commons.io.IOUtils.toByteArray(input)
        } catch (e : Exception) {
            data = null
        }

        if (input != null)
            input.close()

        return data
    }



    fun getFileFromURL(imageUrl: String): ByteArray? {
        android.util.Log.d("ALPERCAKAN", "$imageUrl req")
        try {

            val url = java.net.URL(imageUrl)
            val connection = url.openConnection() as java.net.HttpURLConnection
            connection.doInput = true
            connection.connect()

            android.util.Log.d("ALPERCAKAN", "$imageUrl req OK")
            return org.apache.commons.io.IOUtils.toByteArray(connection.inputStream)

        } catch (e: java.io.IOException) {
            android.util.Log.d("ALPERCAKAN", "$imageUrl req fail")
            return null
        }

    }
}
